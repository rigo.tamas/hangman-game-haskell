{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad (guard, when)
import Control.Monad.State
  ( MonadIO(liftIO)
  , StateT
  , evalStateT
  , execStateT
  , get
  , lift
  , put
  , runState
  )
import Control.Monad.Trans.Except (Except, ExceptT, runExceptT)
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)

import Control.Monad.Error.Class (liftEither)
import Control.Monad.RWS (gets, modify)
import Data.Aeson (Array)
import Data.Aeson.Encoding (encodingToLazyByteString)
import Data.Char (isUpper, toLower)
import GHC.RTS.Flags (MiscFlags(ioManager))
import qualified MyLib (someFunc)
import Network.HTTP.Req
  ( GET(GET)
  , JsonResponse
  , NoReqBody(NoReqBody)
  , (/:)
  , (=:)
  , defaultHttpConfig
  , https
  , jsonResponse
  , req
  , responseBody
  , runReq
  )

data HangmanState =
  HangmanState
    { currentLives :: Int
    , hangmanWord :: String
    }

getUserInput :: IO Char
getUserInput = do
  putStrLn "Guess a letter"
  input <- getLine
  let parsedInput = parseInput input
  case parsedInput of
    Left errorMessage -> putStrLn errorMessage >> getUserInput
    Right validValue -> return validValue

updateGameState :: String -> Bool -> Char -> HangmanState -> HangmanState
updateGameState wordsToGuess match guessedChar hangmanState
  | match =
    HangmanState
      { currentLives = currentLives hangmanState
      , hangmanWord =
          updateHangmanWord wordsToGuess guessedChar $ hangmanWord hangmanState
      }
  | otherwise =
    HangmanState
      { currentLives = currentLives hangmanState - 1
      , hangmanWord = hangmanWord hangmanState
      }

isGameOver :: HangmanState -> Either String ()
isGameOver state
  | currentLives state == 0 = Left "Sorry, you lost"
  | otherwise = Right ()

didWinGame :: String -> HangmanState -> Either String ()
didWinGame wordsToGuess state
  | hangmanWord state == wordsToGuess = Left "You won!"
  | otherwise = Right ()

parseInput :: String -> Either String Char
parseInput input
  | length input /= 1 = Left "Please give exactly one character\n"
  | input == " " = Left "Input cannot be space\n"
  | otherwise = Right $ toLower . head $ input

didMatch :: String -> Char -> Bool
didMatch wordsToGuess guessedChar = guessedChar `elem` wordsToGuess

updateHangmanWord :: String -> Char -> String -> String
updateHangmanWord wordsToGuess guessedChar = zipWith (curry update) wordsToGuess
  where
    update (charToGuess, hangmanChar) =
      if guessedChar == charToGuess
        then charToGuess
        else hangmanChar

printLivesRemainingOrGameState ::
     Bool -> StateT HangmanState (ExceptT String IO) ()
printLivesRemainingOrGameState match = do
  hangmanState <- get
  (if match
     then liftIO $ putStrLn $ hangmanWord hangmanState
     else liftIO $
          putStrLn $
          "Ouch, lives remaining: " ++ (show . currentLives $ hangmanState))

getListOfWords :: Int -> IO [String]
getListOfWords numberOfWords =
  runReq defaultHttpConfig $ do
    result <-
      req
        GET
        (https "random-word-api.herokuapp.com" /: "word")
        NoReqBody
        jsonResponse
        ("number" =: numberOfWords)
    return (responseBody result :: [String])

getInitialGameState :: Int -> String -> HangmanState
getInitialGameState maxLives wordToGuess =
  HangmanState
    { hangmanWord =
        [ if c == ' '
          then ' '
          else '_'
        | c <- wordToGuess
        ]
    , currentLives = maxLives
    }

playing :: String -> HangmanState -> ExceptT String IO ()
playing wordsToGuess = evalStateT loop
  where
    loop :: StateT HangmanState (ExceptT String IO) ()
    loop = do
      guessedChar <- liftIO getUserInput
      let match = didMatch wordsToGuess guessedChar
      modify (updateGameState wordsToGuess match guessedChar)
      printLivesRemainingOrGameState match
      gets isGameOver >>= liftEither
      gets (didWinGame wordsToGuess) >>= liftEither
      loop

main :: IO ()
main = do
  listOfWords <- getListOfWords 2
  let wordsToGuess = unwords listOfWords
  let initialGameState = getInitialGameState 5 wordsToGuess
  putStrLn "Welcome to the Hangman game:"
  putStrLn $ hangmanWord initialGameState
  termination <- runExceptT $ playing wordsToGuess initialGameState
  case termination of
    Left val -> putStrLn val >> putStrLn "Solution was: " >> print wordsToGuess
    Right val -> return ()
